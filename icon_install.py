#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import subprocess as sp

username = sp.check_output('whoami', shell=True)
username = username.decode('utf-8').strip('\n')

if isinstance(username, list):
    username = username[0]
else:
    username = username

script_path = os.path.dirname(os.path.realpath(__file__))
path = f"{script_path}/Asteroids_ARM.x86"
install_path = f"/home/{username}/.local/share/applications/asteroid-belt.desktop"

icon_file = f"""[Desktop Entry]
Name=Asteroid Belt
Comment=Endless runner type game for the PP
Type=Game
Icon={script_path}/logo.svg
Exec={path}
Categories=GTK;Audio;Player;
X-Purism-FormFactor=Workstation;Mobile;
"""

with open(install_path, "w+") as f:
    f.write(icon_file)
